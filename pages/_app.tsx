import '@/styles/globals.css';
import styles from '@/styles/Home.module.css';
import { Inter } from 'next/font/google';
import type { AppProps } from 'next/app';
import Link from 'next/link';

const inter = Inter({ subsets: ['latin'] });

export default function App({ Component, pageProps }: AppProps) {
  return (
    <main className={styles.main}>
      <Component {...pageProps} />

      <div className={styles.grid}>
        <Link className={styles.card} href="/questions/1">
          <h2 className={inter.className}>Question 1</h2>
          <p className={inter.className}>
            What do you want to learn or do more of at work?
          </p>
        </Link>

        <Link className={styles.card} href="/questions/2">
          <h2 className={inter.className}>Question 2</h2>
          <p className={inter.className}>
            Describe how you solved a challenge that one of your previous
            support teams faced. How did you determine your solution was
            successful?
          </p>
        </Link>

        <Link className={styles.card} href="/questions/3">
          <h2 className={inter.className}>Question 3</h2>
          <p className={inter.className}>
            How would you compare Next.js with another framework? Feel free to
            compare with a framework of your choice.
          </p>
        </Link>

        <Link className={styles.card} href="/questions/4">
          <h2 className={inter.className}>Question 4</h2>
          <p className={inter.className}>
            When would you choose to use Edge Functions, Serverless Functions,
            or Edge Middleware with Vercel?
          </p>
        </Link>

        <Link className={styles.card} href="/questions/5">
          <h2 className={inter.className}>Question 5</h2>
          <p className={inter.className}>
            Imagine a customer writes in, requesting help with the following
            question. Write a first response for triaging this case and helping
            them solve the issue.
          </p>
        </Link>
      </div>
    </main>
  );
}
