import Head from 'next/head';
import { Inter } from 'next/font/google';
import styles from '@/styles/Questions.module.css';

const inter = Inter({ subsets: ['latin'] });

export default function Question4() {
  return (
    <>
      <Head>
        <title>Question 4</title>
      </Head>

      <div className={`${inter.className} ${styles.questions}`}>
        <h1 className={styles.heading}>
          When would you choose to use Edge Functions, Serverless Functions, or
          Edge Middleware with Vercel?
        </h1>
        <p>
          Ultimately, the choice between Edge Functions, Serverless Functions,
          or Edge Middleware will depend on the specific needs of your project
          and what you&apos;re trying to achieve. However, there are some
          general guidelines that can help you decide which approach is best for
          the specific use case.
        </p>
        <p>
          <span className={styles.vercelFeature}>Edge Functions</span> - Usually
          used for lightweight tasks that can be performed quickly and
          don&apos;t require a lot of resources. Edge Functions are a great
          choice for tasks like: authentication, logging, and personalized
          content.
        </p>

        <p>
          <span className={styles.vercelFeature}>Edge Middleware</span> -
          Similar to Edge Functions, Edge Middleware is used for lightweight
          tasks that can be performed quickly and don&apos;t require a lot of
          resources. However, Edge Middleware is a great choice for tasks that
          require access to the request and response objects, like: A/B testing,
          redirects, and custom headers.
        </p>

        <p>
          <span className={styles.vercelFeature}>Serverless Functions</span> -
          Used for for more complex tasks that require more resources and can
          take a bit longer to execute than Edge Functions. Serverless Functions
          are a great choice for tasks like: data processing (like form
          submissions), image manipulation, and API requests.
        </p>
      </div>
    </>
  );
}
