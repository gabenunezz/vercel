import Head from 'next/head';
import { Inter } from 'next/font/google';
import styles from '@/styles/Questions.module.css';

const inter = Inter({ subsets: ['latin'] });

export default function Question1() {
  return (
    <>
      <Head>
        <title>Question 1</title>
      </Head>

      <div className={`${inter.className} ${styles.questions}`}>
        <h1 className={styles.heading}>
          What do you want to learn or do more of at work?
        </h1>
        <p>
          I&apos;m passionate about being client-facing and effectively
          communicating with both technical and non-technical stakeholders.
          I&apos;d like to continue honing these skills in my role and take on
          more challenging projects that allow me to interact with clients and
          team members. As an advocate for clients, I believe it&apos;s crucial
          to deeply understand their needs, concerns, and goals, and to ensure
          that their voice is heard throughout their development process.
        </p>
        <p>
          As a developer, I strive to continuously improve my skills and stay
          up-to-date with the latest industry trends and technologies. I&apos;m
          eager to learn new programming languages, frameworks, and tools to
          enhance my development abilities and create even better solutions for
          our clients.
        </p>
        <p>
          I&apos;m particularly interested in the ways we can optimize web
          performance, improve security, and enhance user experience. I&apos;m
          excited to learn more about these areas and contribute to our
          team&apos;s efforts to create fast, secure, and user-friendly web
          applications.
        </p>
        <p>
          Lastly, I&apos;m keen to expand my leadership skills and take on more
          responsibilities in this area. I&apos;m interested in exploring
          opportunities to progress my career and contribute to the growth and
          success of the company.
        </p>
      </div>
    </>
  );
}
