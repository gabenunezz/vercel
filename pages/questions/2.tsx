import Head from 'next/head';
import { Inter } from 'next/font/google';
import styles from '@/styles/Questions.module.css';

const inter = Inter({ subsets: ['latin'] });

export default function Question2() {
  return (
    <>
      <Head>
        <title>Question 2</title>
      </Head>

      <div className={`${inter.className} ${styles.questions}`}>
        <h1 className={styles.heading}>
          Describe how you solved a challenge that one of your previous support
          teams faced. <br /> How did you determine your solution was
          successful?
        </h1>
        <p>
          At Shogun, I worked on our product, Shogun Frontend, an all-in-one
          headless eCommerce frontend provider. One of the challenges we faced
          was the inability to perform a &quot;selective copy&quot; of content
          groups with their items within our native built-in CMS from one
          Frontend storefront to another. This was a highly requested feature
          for dozens of clients.
        </p>
        <p>
          To address this challenge, I decided to build a custom script that
          would take care of all the complex API calls needed to get the data
          from one store to another. I did this without relying on engineering
          to step in, relying solely on my ability to find the API calls I
          needed. This involved understanding the intricacies of our CMS and how
          it interacted with our API.
        </p>
        <p>
          After building the script, I tested it thoroughly, making sure it
          could handle all the different content groups and items that clients
          were using. Once I was confident it was working as intended, I started
          using it for clients who had requested the feature. The feedback was
          positive, and clients were happy they could now easily copy content
          groups between storefronts.
        </p>
        <p>
          I determined the solution was ultimately a success after dozens of
          tests and using it for a few clients when needed. It was fulfilling to
          see that my efforts could help make our clients&apos; work easier and
          more efficient.
        </p>
      </div>
    </>
  );
}
