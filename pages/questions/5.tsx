import Head from 'next/head';
import { Inter } from 'next/font/google';
import styles from '@/styles/Questions.module.css';

const inter = Inter({ subsets: ['latin'] });

export default function Question5() {
  return (
    <>
      <Head>
        <title>Question 5</title>
      </Head>

      <div className={`${inter.className} ${styles.questions}`}>
        <h1 className={styles.heading}>
          Imagine a customer writes in, requesting help with the following
          question. Write a first response for triaging this case and helping
          them solve the issue.
        </h1>
        <div className={styles.questionBox}>
          <h2>
            Hi there, I keep getting a build error from Vercel saying that
            “/vercel/path0/dist/routes-manifest.json” couldn&apos;t be found.
            Can you help me debug this?? I&apos;m so frustrated. I&apos;ve been
            trying to make this work for hours and I just can&apos;t figure it
            out. Your docs aren&apos;t helpful.
          </h2>
        </div>
        <p>My initial response for this ticket that would come in would be:</p>
        <p>Hey there [name],</p>
        <p>
          Thanks for reaching out! What have you tried thus far to try and solve
          this and when did it begin popping up? I&apos;m happy to help you get
          this resolved.
        </p>
        <div className={styles.questionBox}>
          <ol>
            <li>
              What do you think is one of the most common problems which
              customers ask Vercel for help with? How would you help customers
              to overcome common problems, short-term and long-term?
            </li>
            <li>
              How could we improve or alter this familiarization exercise?
            </li>
          </ol>
        </div>
        <p>
          1. From my point of view, I feel that the #1 thing customers are
          reaching out about would initial setup and configuration, as well as
          any third party integrations.
        </p>
        <p>
          2. I think this is a great exercise to get started and to further
          understand Vercel and Next.js :) Perhaps a little bit more focus on
          understanding whom uses Vercel and why would be helpful!
        </p>
      </div>
    </>
  );
}
